# vue2

## 简介

> 在vue2项目前后端分离的情况下，加入微信扫码登录，利用iframe嵌套微信二维码页面  
> 有不懂的可以加我微信号yizheng369

## Look
![效果](./src/assets/login_vue2.jpg)
![效果](./src/assets/home.jpg)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
