const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer:{
    host: 'www.txjava.cn',
    port: 80,
    proxy:{
      "/api":{
        target: 'http://localhost:90',
        changeOrigin: true,
        pathRewrite:{
          '^/api':''
        }
      }
    }
  }
})
